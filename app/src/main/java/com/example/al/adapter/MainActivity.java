package com.example.al.adapter;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends Activity {

	String[] listData = {"Item 1", "Item 2", "Item 3"};
//	Integer[] listDataI = {10, 20, 30};

	ListView lv;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		lv = (ListView)findViewById(R.id.listView);
		lv.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
		ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.list_item, listData);
//		ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, listData);
//		ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_single_choice, listData);
		lv.setAdapter(adapter);
//		ArrayAdapter<Integer> adapter2 = new ArrayAdapter(this, R.layout.list_item_i, listDataI);
//		lv.setAdapter(adapter2);

		lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
				Toast.makeText(getApplicationContext(), "position: " + i + "\nid: " + l, Toast.LENGTH_SHORT).show();
			}
		});

		findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				StringBuilder res = new StringBuilder();
				SparseBooleanArray b = lv.getCheckedItemPositions();
				for (int i = 0; i < b.size(); i++) {
					int key = b.keyAt(i);
					if ( b.get(key) ) {
						res.append(listData[key]).append("\n");
					}
				}
				Toast.makeText(getApplicationContext(), res.toString(), Toast.LENGTH_SHORT).show();
				Log.i("_LOG_", res.toString());
			}
		});
	}
}
